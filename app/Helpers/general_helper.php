<?php

if (!function_exists('jsonPost')) {
    function jsonPost($options): \CodeIgniter\HTTP\ResponseInterface
    {
        // Load the library
        $client = \Config\Services::curlrequest();

        // Make the request
        return $client->request('POST', $options['path'], [
            'json' => $options['data']
        ]);
    }
}