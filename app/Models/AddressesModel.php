<?php namespace App\Models;

class AddressesModel extends BaseModel
{
    protected $table = 'Addresses';
    protected $primaryKey = 'AddressId';
    protected $returnType = 'array';
    protected $allowedFields = ['CustomerId', 'Street', 'Number', 'ZipCode', 'City'];

    public function saveData($data, $nextStep, $customerIdCookie, $cookieTime): array
    {
        try {
            // Check if there's the CustomerId cookie
            if (!$customerIdCookie) {
                // Return next step
                return $this->OK(['nextStep' => $this->defaultStep], 200);
            }

            // Start transaction
            $this->db->transStart();

            // Get step or matched condition
            $step = $this->stepEqualsValue($customerIdCookie, $nextStep - 1);

            // Validate if we are in the correct step
            if ($step !== true) {
                // Return redirect
                return $this->OK(['nextStep' => $step], 200);
            }

            // Save the address data
            $data = ['CustomerId' => $customerIdCookie] + $data;
            $this->insert($data);

            // Update data for the customer's account
            $registrationsModel = model('App\Models\RegistrationsModel');
            $registrationsModel->updateData([
                'where' => [
                    'CustomerId' => $customerIdCookie,
                ],
                'set' => [
                    'Step' => $nextStep,
                ]
            ]);

            // End transaction
            $this->db->transComplete();

            // Set cookie
            set_cookie('CustomerId', $customerIdCookie, $cookieTime);

            // Return success
            return $this->OK(['nextStep' => $nextStep, 'info' => $data], 201);
        } catch (\Exception $exception) {
            // Return the error
            return $this->Error(['error' => 'There has been an error inserting the data.'], 500);
        }
    }
}