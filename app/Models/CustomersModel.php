<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Validation\ValidationInterface;

class CustomersModel extends BaseModel
{
    protected $table = 'Customers';
    protected $primaryKey = 'CustomerId';
    protected $returnType = 'array';
    protected $allowedFields = ['Firstname', 'Lastname', 'Telephone'];

    public function __construct(ConnectionInterface &$db = null, ValidationInterface $validation = null)
    {
        parent::__construct($db, $validation);
        $this->builder = $this->builder();
    }

    public function saveData($data, $nextStep, $customerIdCookie, $cookieTime): array
    {
        try {
            // Start transaction
            $this->db->transStart();

            // Load Registrations model
            $registrationsModel = model('App\Models\RegistrationsModel');

            // Check if there's the CustomerId cookie
            if ($customerIdCookie) {
                // Get step if exists
                $step = $registrationsModel->getStep($customerIdCookie);

                // Check if the customer has already started the registration process
                if ($step > 0) {
                    // Return redirect
                    return $this->OK(['nextStep' => $step], 200);
                }
            }

            // Save the customer's data & get last inserted ID
            $this->insert($data);
            $lastId = $this->db->insertID();

            // Insert the data for the registrations table
            $registrationsModel->save([
                'CustomerId' => $lastId,
                'Step' => $nextStep,
            ]);

            // End transaction
            $this->db->transComplete();

            // Set cookie
            set_cookie('CustomerId', $lastId, $cookieTime);

            // Return success
            $data = ['CustomerId' => $lastId] + $data;
            return $this->OK(['nextStep' => $nextStep, 'info' => $data], 201);
        } catch (\Exception $exception) {
            // Return the error
            return $this->Error(['error' => 'There has been an error inserting the data.'], 500);
        }
    }
}