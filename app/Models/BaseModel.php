<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Validation\ValidationInterface;

class BaseModel extends Model
{
    protected $defaultStep = 1;

    public function __construct(ConnectionInterface &$db = null, ValidationInterface $validation = null)
    {
        parent::__construct($db, $validation);
    }

    protected function OK($data, $code): array
    {
        return [
            'result' => 'OK',
            'data' => $data,
            'code' => $code
        ];
    }

    protected function Error($data, $code): array
    {
        return [
            'result' => 'Error',
            'data' => $data,
            'code' => $code
        ];
    }

    protected function stepEqualsValue($customerId, $currentStep)
    {
        // Get step if exists
        $registrationsModel = model('App\Models\RegistrationsModel');
        $step = $registrationsModel->getStep($customerId);

        // Check if steps match
        if ($step != $currentStep) {
            // Return the step
            return $step;
        }

        return true;
    }
}