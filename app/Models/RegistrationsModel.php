<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Validation\ValidationInterface;

class RegistrationsModel extends BaseModel
{
    protected $table = 'Registrations';
    protected $primaryKey = 'RegistrationId';
    protected $returnType = 'array';
    protected $allowedFields = ['CustomerId', 'Step'];

    public function __construct(ConnectionInterface &$db = null, ValidationInterface $validation = null)
    {
        parent::__construct($db, $validation);
        $this->builder = $this->builder();
    }

    public function getStep($customerId): int
    {
        // Find element
        $registration = $this->builder
            ->select('Step')
            ->getWhere([
                'CustomerId' => $customerId
            ]);
        $registration = $registration->getResultArray();

        // Return value
        return count($registration) > 0 ? $registration[0]['Step'] : 0;
    }

    public function updateData($options): void
    {
        $this->builder->set($options['set']);
        $this->builder->where($options['where']);
        $this->builder->update();
    }

    public function getCurrentStep($customerIdCookie, $cookieTime): array
    {
        try {
            // Check if there's the CustomerId cookie
            if (!$customerIdCookie) {
                // Return next step
                return $this->OK(['step' => $this->defaultStep], 200);
            }

            // Set cookie
            set_cookie('CustomerId', $customerIdCookie, $cookieTime);

            // Get step
            $step = $this->getStep($customerIdCookie);
            $step = $step > 0 ? $step : $this->defaultStep;

            // Return the step
            return $this->OK(['nextStep' => $step], 200);
        } catch (\Exception $exception) {
            // Return the error
            return $this->Error(['error' => 'There has been an error retrieving the data.'], 500);
        }
    }
}