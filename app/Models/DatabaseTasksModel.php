<?php namespace App\Models;

class DatabaseTasksModel extends BaseModel
{
    public function createInitialResources($database): array
    {
        $forge = \Config\Database::forge();

        try {
            // Create database
            $forge->createDatabase($database, true);

            // Customers table
            $fields = [
                'CustomerId' => [
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => true
                ],
                'Firstname' => [
                    'type' => 'VARCHAR',
                    'constraint' => 40,
                ],
                'Lastname' => [
                    'type' => 'VARCHAR',
                    'constraint' => 40,
                ],
                'Telephone' => [
                    'type' => 'TINYINT',
                    'constraint' => 50,
                    'unsigned' => true,
                ],
            ];
            $forge->addField($fields);
            $forge->addPrimaryKey('CustomerId');
            $forge->createTable('Customers', true);

            // Addresses table
            $fields = [
                'AddressId' => [
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => true
                ],
                'CustomerId' => [
                    'type' => 'INT',
                    'constraint' => 11,
                ],
                'Street' => [
                    'type' => 'VARCHAR',
                    'constraint' => 40,
                ],
                'Number' => [
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                ],
                'ZipCode' => [
                    'type' => 'CHAR',
                    'constraint' => 50,
                ],
                'City' => [
                    'type' => 'VARCHAR',
                    'constraint' => 200,
                ],
            ];
            $forge->addField($fields);
            $forge->addPrimaryKey('AddressId');
            $forge->addForeignKey('CustomerId','Customers','CustomerId');
            $forge->createTable('Addresses', true);

            // Accounts table
            $fields = [
                'AccountId' => [
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => true
                ],
                'CustomerId' => [
                    'type' => 'INT',
                    'constraint' => 11,
                ],
                'OwnerName' => [
                    'type' => 'VARCHAR',
                    'constraint' => 40,
                ],
                'Iban' => [
                    'type' => 'VARCHAR',
                    'constraint' => 100,
                ],
                'PaymentDataId' => [
                    'type' => 'CHAR',
                    'constraint' => 200,
                ],
            ];
            $forge->addField($fields);
            $forge->addPrimaryKey('AccountId');
            $forge->addForeignKey('CustomerId','Customers','CustomerId');
            $forge->createTable('Accounts', true);

            // Registrations table
            $fields = [
                'RegistrationId' => [
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => true
                ],
                'CustomerId' => [
                    'type' => 'INT',
                    'constraint' => 11,
                ],
                'Step' => [
                    'type' => 'TINYINT',
                    'constraint' => 10,
                    'unsigned' => true,
                ],
            ];
            $forge->addField($fields);
            $forge->addPrimaryKey('RegistrationId');
            $forge->addForeignKey('CustomerId','Customers','CustomerId');
            $forge->createTable('Registrations', true);

            // Return success
            return $this->OK(['message' => 'The database resources have been initialized correctly.'], 201);
        } catch (\Exception $exception) {
            // Return error
            return $this->Error(['error' => 'The database could not be initialized.'], 500);
        }
    }
}