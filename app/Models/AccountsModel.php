<?php namespace App\Models;

class AccountsModel extends BaseModel
{
    protected $table = 'Accounts';
    protected $primaryKey = 'AccountId';
    protected $returnType = 'array';
    protected $allowedFields = ['CustomerId', 'OwnerName', 'Iban', 'PaymentDataId'];
    protected $externalApiPath = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    public function saveData($data, $nextStep, $customerIdCookie, $cookieTime): array
    {
        try {
            // Check if there's the CustomerId cookie
            if (!$customerIdCookie) {
                // Return next step
                return $this->OK(['nextStep' => $this->defaultStep], 200);
            }

            // Start transaction
            $this->db->transStart();

            // Get step or matched condition
            $step = $this->stepEqualsValue($customerIdCookie, $nextStep - 1);

            // Validate if we are in the correct step
            if ($step !== true) {
                // Return redirect
                return $this->OK(['nextStep' => $step], 200);
            }

            // Call the external API
            $response = jsonPost([
                'path' => $this->externalApiPath,
                'data' => [
                    'customerId' => $customerIdCookie,
                    'iban' => $data['Iban'],
                    'owner' => $data['OwnerName']
                ],
            ]);

            // Get first number of response code
            $responseCode = $response->getStatusCode();
            $firstNumber = substr($responseCode, 0, 1);

            // Check if it is an 4xx error
            if ($firstNumber == 4) {
                // Respond with error
                return $this->Error(['error' => 'The external API responded with errors, please try again later.'], 400);
            }

            // Get the data from the external API
            $externalApiData = json_decode($response->getBody(), true);
            $paymentDataId = $externalApiData['paymentDataId'];

            // Save the account data
            $data = [
                'CustomerId' => $customerIdCookie,
                'OwnerName' => $data['OwnerName'],
                'Iban' => $data['Iban'],
                'PaymentDataId' => $paymentDataId
            ];
            $this->insert($data);

            // Update data for the customer's account
            $registrationsModel = model('App\Models\RegistrationsModel');
            $registrationsModel->updateData([
                'where' => [
                    'CustomerId' => $customerIdCookie,
                ],
                'set' => [
                    'Step' => $nextStep,
                ]
            ]);

            // End transaction
            $this->db->transComplete();

            // Set cookie
            set_cookie('CustomerId', $customerIdCookie, $cookieTime);

            // Return success
            return $this->OK(['nextStep' => $nextStep, 'info' => $data], 201);
        } catch (\Exception $exception) {
            // Return the error
            return $this->Error(['error' => 'There has been an error inserting the data.'], 500);
        }
    }

    public function getData($currentStep, $customerIdCookie, $cookieTime): array
    {
        try {
            // Check if there's the CustomerId cookie
            if (!$customerIdCookie) {
                // Return next step
                return $this->OK(['nextStep' => $this->defaultStep], 200);
            }

            // Get step or matched condition
            $step = $this->stepEqualsValue($customerIdCookie, $currentStep);

            // Validate if we are in the correct step
            if ($step !== true) {
                // Return redirect
                return $this->OK(['nextStep' => $step], 200);
            }

            // Get the account data
            $account = $this
                ->where('CustomerId', $customerIdCookie)
                ->first();

            // Set cookie
            set_cookie('CustomerId', $customerIdCookie, $cookieTime);

            // Return success
            return $this->OK(['nextStep' => $currentStep, 'info' => $account], 200);
        } catch (\Exception $exception) {
            // Return the error
            return $this->Error(['error' => 'There has been an error retrieving the data.'], 500);
        }
    }
}