<?php namespace App\Controllers;

class Accounts extends BaseController
{
    public function newRecord(): \CodeIgniter\HTTP\Response
    {
        // Default vars
        $nextStep = 4;

        // Check if provided data is of format JSON
        if (!$post = $this->request->getJSON(true)) {
            // Send error
            return $this->fail([
                'result' => 'Error',
                'data' => [
                    'message' => 'The body of the request must be a valid JSON format.'
                ],
            ]);
        }

        // Save the data
        $accountsModel = model('App\Models\AccountsModel');
        $result = $accountsModel->saveData($post, $nextStep, get_cookie('CustomerId'), $this->cookieTime);

        // Return response
        return $this->respond([
            'result' => $result['result'],
            'data' => $result['data'],
        ], $result['code']);
    }

    public function get(): \CodeIgniter\HTTP\Response
    {
        // Default vars
        $currentStep = 4;

        // Save the data
        $accountsModel = model('App\Models\AccountsModel');
        $result = $accountsModel->getData($currentStep, get_cookie('CustomerId'), $this->cookieTime);

        // Return response
        return $this->respond([
            'result' => $result['result'],
            'data' => $result['data'],
        ], $result['code']);
    }
}
