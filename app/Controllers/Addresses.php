<?php namespace App\Controllers;

class Addresses extends BaseController
{
    public function newRecord(): \CodeIgniter\HTTP\Response
    {
        // Default vars
        $nextStep = 3;

        // Check if provided data is of format JSON
        if (!$post = $this->request->getJSON(true)) {
            // Send error
            return $this->fail([
                'result' => 'Error',
                'data' => [
                    'message' => 'The body of the request must be a valid JSON format.'
                ],
            ]);
        }

        // Save the data
        $addressesModel = model('App\Models\AddressesModel');
        $result = $addressesModel->saveData($post, $nextStep, get_cookie('CustomerId'), $this->cookieTime);

        // Return response
        return $this->respond([
            'result' => $result['result'],
            'data' => $result['data'],
        ], $result['code']);
    }
}
