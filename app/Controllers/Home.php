<?php namespace App\Controllers;

class Home extends BaseController
{
    public function index(): string
    {
        // Return default view
        return view('default_view');
    }

    public function welcome(): \CodeIgniter\HTTP\Response
    {
        // Send welcome message
        return $this->respond([
            'result' => 'OK',
            'data' => [
                'message' => 'Welcome to Registration API '.getenv('app.apiVersion').'.'
            ]
        ]);
    }

    public function initialize(): \CodeIgniter\HTTP\Response
    {
        // Create initial resources for the default and tests database
        $databaseTasksModel = model('App\Models\DatabaseTasksModel');
        $result = $databaseTasksModel->createInitialResources(getenv('database.default.database'));

        // Return response
        return $this->respond([
            'result' => $result['result'],
            'data' => $result['data'],
        ], $result['code']);
    }

    public function show404()
    {
        // Set headers & data
        $this->response->setStatusCode(404);
        $this->response->setContentType('application/json');
        echo json_encode([
            'result' => 'Error',
            'data' => [
                'error' => 'Endpoint not found.'
            ],
        ]);
    }
}
