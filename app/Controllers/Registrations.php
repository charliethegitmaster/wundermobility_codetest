<?php namespace App\Controllers;

class Registrations extends BaseController
{
    public function getCurrentStep(): \CodeIgniter\HTTP\Response
    {
        // Get the data
        $registrationsModel = model('App\Models\RegistrationsModel');
        $result = $registrationsModel->getCurrentStep(get_cookie('CustomerId'), $this->cookieTime);

        // Return response
        return $this->respond([
            'result' => $result['result'],
            'data' => $result['data'],
        ], $result['code']);
    }
}
