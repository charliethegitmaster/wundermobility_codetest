<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registration form</title>
	<meta name="description" content="Registration form pointing an API">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
    <div id="app">
        <h2>Sign Up Form</h2>
        <div id="errors"></div>
        <div id="content"></div>
    </div>

    <!-- Scripts -->
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    <script src="js/main.js"></script>
</body>
</html>