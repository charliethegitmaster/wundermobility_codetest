
# Multi-Step Sign Up Form

Demo to show how to implement a multi-step form.

The frontend interacts with an API and keeps track of users with cookies. 

## TECH STACK

- Codeigniter 4
- Php 7.4
- MySQL 5.7
- Nginx
- PhpMyAdmin
- Docker

## REQUIREMENTS

- Docker
- Docker compose

## INSTALLATION

- Clone this repo

- Go to cloned folder:

    cd WunderMobility_CodeTest


- Set up Docker:
  
    docker-compose build app

    docker-compose up -d


- Check PhpMyAdmin works:

    Go to http://<SERVER_IP>:8080, wait till no connection errors show (2 min max)


- Set up initial database tables (IDEMPOTENT process):
  
    docker-compose exec app php /var/www/html/public/index.php api/v1/initialize


- You're ready to use the form at http://<SERVER_IP>:8000

## NOTES

- If the installation is done via Docker, there's no need to restore the database, but in any case, a dump can be found at <PROJECT_ROOT>/app.sql  

## POSSIBLE PERFORMANCE OPTIMIZATIONS

- Regarding PHP: it could be used PHP 8 with a framework that currently supports it.
- Regarding the database: use of Aurora database (AWS - 3x faster) instead of MySQL
- Regarding the frontend: minimized code using Webpack for faster execution & smaller size
- Regarding the general infrastructure:
  
    ... Frontend: could have been uploaded to S3 for faster retrieval, in conjunction with CloudFront (AWS) for worldwide fast access.
  
    ... Backend: could have been used ECS (AWS) with Fargate launch type (docker containers automatically scaled) with an Application Load Balancer (AWS)

## THINGS THAT COULD HAVE BEEN DONE BETTER

- Frontend: use of a frontend framework (VueJS, React, ...), in conjunction with Babel (for compatibility with older explorers) and Webpack (for minification & bundling)
- Backend: Use of microservices

## PATTERNS USED

- The pattern that has been used is MVC (Model-View-Controller) as it is the default one from Codeigniter 4