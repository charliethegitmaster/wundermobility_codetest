
// Listeners
$(document).ready(main);

// Functions
function main() {
    // Default vars
    const basePath = 'api/v1'
    const endpoints = {
        1: basePath + '/customers/new',
        2: basePath + '/addresses/new',
        3: basePath + '/accounts/new',
        4: basePath + '/accounts/get',
        'currentStep': basePath + '/steps/current'
    }
    const data = {
        lastStep: 4,
        endpoints,
        selectorOK: '#content',
        selectorError: '#errors'
    }

    // Fill the content with a loader text
    $(data.selectorOK).html('Loading...Please wait.')

    // Check if cookie exists
    if (document.cookie.indexOf('CustomerId') !== -1) {
        // Get current step
        $.get(endpoints.currentStep, response => responseOK(response, data))
        .fail(response => responseError(response, data))
        return
    }

    // Load first step
    loadTemplate(data.selectorOK, getTemplate({ nextStep: 1 }), data)
}
function loadListeners(data) {
    // Form listener
    $('#form').on('submit', event => {
        // Prevent form from getting executed
        event.preventDefault()

        // Get the data
        const step = $("#form").attr('data-step')
        const formData = $("#form :input").serializeArray().reduce((obj, field) => {
            obj[field.name] = field.value
            return obj
        }, {})
        const jsonData = JSON.stringify(formData, null, ' ')

        // Call the endpoint
        $.post(data.endpoints[step], jsonData, response => responseOK(response, data))
        .fail(response => responseError(response, data))
  })
}
function insertError(selector, data) {
    $(selector).html(`<h5 class='error'>${data}</h5>`)
}
function insertData(selector, data) {
    $(selector).html(data)
}
function getTemplate(data) {
    // Default vars
    let template = ''
  
    // switch steps
    switch (data.nextStep) {
        case 2:
            template = `
            <h3>---------- Step ${data.nextStep} ----------</h3>
            <form id='form' data-step='${data.nextStep}'>
                <label>Street</label>
                <br><br>
                <input type='text' name='Street' placeholder='Wall Street' required maxlength=40>
                <br><br>
                <label>Number</label>
                <br><br>
                <input type='number' name='Number' placeholder='125' required maxlength=11 step=1>
                <br><br>
                <label>ZipCode</label>
                <br><br>
                <input type='text' name='ZipCode' placeholder='10005' required maxlength=50 step=1>
                <br><br>
                <label>City</label>
                <br><br>
                <input type='text' name='City' placeholder='New York' required maxlength=200>
                <br><br>
                <input type='submit' value='Send'>
            </form>`
            break
        case 3:
            template = `
                <h3>---------- Step ${data.nextStep} ----------</h3>
                <form id='form' data-step='${data.nextStep}'>
                    <label>Owner Name</label>
                    <br><br>
                    <input type='text' name='OwnerName' placeholder='Tim Berners Lee' required maxlength=40>
                    <br><br>
                    <label>Iban</label>
                    <br><br>
                    <input type='text' name='Iban' placeholder='DE89 3704 0044 0532 0130 00' required maxlength=100>
                    <br><br>
                    <input type='submit' value='Send'>
                </form>`
            break
        case 4:
            template = `
            <h3>---------- Step ${data.nextStep} ----------</h3>
            <div><b>Success!</b> - The information was saved correctly.</div>
            <br>
            <div><b>Payment ID</b>: ${data.info.PaymentDataId}.</div>`
            break
        default:
            template = `
            <h3>---------- Step ${data.nextStep} ----------</h3>
            <form id='form' data-step='${data.nextStep}'>
                <label>Firstname</label>
                <br><br>
                <input type='text' name='Firstname' placeholder='John' required maxlength=40>
                <br><br>
                <label>Lastname</label>
                <br><br>
                <input type='text' name='Lastname' placeholder='Smith' required maxlength=40>
                <br><br>
                <label>Telephone</label>
                <br><br>
                <input type='tel' name='Telephone' placeholder='123456789' required maxlength=50 pattern="[0-9]{0,11}">
                <br><br>
                <input type='submit' value='Send'>
            </form>`
    }

    // Return template
    return template
}
function loadTemplate(selector, template, data) {
    insertData(selector, template)
    loadListeners(data)
}
function responseOK(response, data) {
    // Check if we are the last step
    if (response.data.nextStep === data.lastStep) {
        // Do query to get payment ID data
        $.get(data.endpoints[data.lastStep], response => insertData(data.selectorOK, getTemplate(response.data)))
        .fail(response => responseError(response, data))
        return
    }

    // Load next step
    loadTemplate(data.selectorOK, getTemplate(response.data), data)
}
function responseError(response, data) {
    // Insert error
    insertError(data.selectorError, response.responseJSON['data']['error'])
}